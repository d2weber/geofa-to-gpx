# GeoFA to GPX

This project aims to export the map markers from [udinaturen.dk](https://udinaturen.dk/) to a GPX file.
The GPX file can be imported for example in OsmAnd to be used offline.
Currently the shelters and tent spots will be shown in two folders in the favourites.
The GPX file can be [downloaded](https://gitlab.com/d2weber/geofa-to-gpx/-/jobs/artifacts/master/raw/GeoFA_export.gpx?job=run:cargo) as an artifact from the pipeline.

To access the corresponding markers, the SQL interface of GeoFA is used.
The documentation can be found on [geodenmark.dk](https://www.geodanmark.dk/wp-content/uploads/2019/10/GeoFA-SQL-API-vejledning-version-1.0-220421.pdf).

