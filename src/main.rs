use geojson::GeoJson::FeatureCollection;
use gpx::{write, Gpx, GpxVersion};
use std::fs::File;

mod feature_to_waypoint;
use feature_to_waypoint::to_waypoint;

mod geofa;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let mut data: Gpx = Default::default();
    data.version = GpxVersion::Gpx11;
    data.metadata = Some(gpx::Metadata {
        name: Some("favourites".into()),
        ..Default::default()
    });
    let geojson = geofa::aquire_geojson("SELECT navn,facil_ty,beskrivels,geometri FROM fkg.t_5800_fac_pkt WHERE facil_ty LIKE 'Teltplads' OR facil_ty LIKE 'Shelter'")?;

    let features = match geojson {
        FeatureCollection(c) => c.features,
        _ => panic!("We expected FeatureCollection"),
    };
    for f in features {
        data.waypoints.push(to_waypoint(&f))
    }
    {
        let file = File::create("GeoFA_export.gpx").unwrap();
        write(&data, file).unwrap();
    }
    Ok(())
}
