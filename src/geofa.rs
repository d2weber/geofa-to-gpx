use geojson::GeoJson;

pub fn aquire_geojson(query: &str) -> Result<geojson::GeoJson, Box<dyn std::error::Error>> {
    let resp = reqwest::blocking::get(format!(
        "https://geofa.geodanmark.dk/api/v2/sql/fkg?q={}&srs=4326",
        query
    ))?
    .text()?;
    Ok(resp.parse::<GeoJson>().unwrap())
}

#[cfg(test)]
mod tests {
    use super::*;
    use geojson::{GeoJson::FeatureCollection, Geometry, Value::MultiPoint};

    #[test]
    fn geofa_is_available() -> Result<(), Box<dyn std::error::Error>> {
        reqwest::blocking::get("https://geofa.geodanmark.dk")?;
        Ok(())
    }

    #[test]
    fn query_gives_geojson() -> Result<(), Box<dyn std::error::Error>> {
        let geojson = aquire_geojson("SELECT navn,geometri FROM fkg.t_5800_fac_pkt LIMIT 3")?;

        let features = match geojson {
            FeatureCollection(c) => c.features,
            _ => panic!("We expected FeatureCollection"),
        };
        assert_eq!(features.len(), 3);
        match &features[0] {
            geojson::Feature {
                bbox: None,
                geometry: Some(_),
                id: None,
                properties: Some(_),
                foreign_members: None,
            } => (),
            _ => panic!("Fileds in Feature are not as expected"),
        };
        match &features[0].geometry {
            Some(Geometry {
                value: MultiPoint(_),
                ..
            }) => (),
            _ => panic!("Is not a MultiPoint"),
        };

        &features[0].property("navn").unwrap().as_str().unwrap();

        Ok(())
    }
}


