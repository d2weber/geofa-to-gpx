use geojson::{Geometry, Value::MultiPoint};
use gpx::Waypoint;

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_feature_to_waypoint() {
        let properties = serde_json::to_value(Properties {
            navn: Some("myName".into()),
            beskrivels: Some("myDescription".into()),
            facil_ty: "myType".into(),
        })
        .unwrap()
        .as_object()
        .unwrap()
        .clone();
        let f = geojson::Feature {
            bbox: None,
            geometry: Some(Geometry {
                bbox: None,
                value: MultiPoint(vec![vec![1., 2.]]),
                foreign_members: None,
            }),
            id: None,
            properties: Some(properties),
            foreign_members: None,
        };

        let w = to_waypoint(&f);
        assert_eq!(w.point(), geo::Point(geo::Coordinate { x: 1., y: 2. }));
        assert_eq!(w.name, Some("myName".into()));
        assert_eq!(w.comment, Some("myDescription".into()));
        assert_eq!(w._type, Some("GeoFA myType".into()));
    }
}

use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize)]
struct Properties {
    navn: Option<String>,
    beskrivels: Option<String>,
    facil_ty: String,
}

pub fn to_waypoint(f: &geojson::Feature) -> Waypoint {
    let p = match &f.geometry {
        Some(Geometry {
            value: MultiPoint(p),
            ..
        }) => p,
        _ => panic!("No MultiPoint"),
    };
    if p.len() > 1 {
        println!("Found length {}", p.len());
    }
    let p = &p[0];

    let mut waypoint = Waypoint::new(geo::Point(geo::Coordinate { x: p[0], y: p[1] }));
    let prop: Properties =
        serde_json::from_value(serde_json::Value::Object(f.properties.clone().unwrap())).unwrap();
    waypoint.name = prop.navn;
    waypoint.comment = prop.beskrivels;
    waypoint._type = Some(format!("GeoFA {}", prop.facil_ty));
    waypoint
}

